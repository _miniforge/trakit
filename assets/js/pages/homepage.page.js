parasails.registerPage('homepage', {
  // initial state
  data: {
    heroHeightSet: false,
    curDateCalc: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0, 10),
    testJson: [["Joe", "11:50", "22:15", "Feeding the birds", "07-30-2018"], ["Donald", "01:00", "13:05", "Unboxing boxes", "07-20-2018"]],
    currentRow: 1,
  },

  // Lifecycle
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },

  mounted: async function(){
    this._setHeroHeight();

    var testKeyResult = window.localStorage.getItem('employees');
    if (testKeyResult !== null) {
      this.testJson = JSON.parse(testKeyResult);
      for (let i = 0; i < this.testJson.length; i++) {
        this.addRowForEmployee(
          this.testJson[i][0],
          this.testJson[i][1],
          this.testJson[i][2],
          this.testJson[i][3],
          this.testJson[i][4],
        );
      }
    }
  },

  // interactions
  methods: {

    addNewEmployee: function() {
      console.log("Hello world!");

      // get the values from the Modal
      var formNameVal = $("#employeeName").val();
      var formTimeInVal = $("#timein").val();
      var formTimeOutVal = $("#timeout").val();
      var formActivityVal = $("#activity").val();
      var formCurDateVal = $("#curDate").val();

      this.testJson.push([formNameVal, formTimeInVal, formTimeOutVal, formActivityVal, formCurDateVal]);
      this.clearModal();
      this.addRowForEmployee(formNameVal, formTimeInVal, formTimeOutVal, formActivityVal, formCurDateVal);

      $("#exampleModal").modal('hide');

      window.localStorage.setItem('employees', JSON.stringify(this.testJson));
    },

    addRowForEmployee: function(name, timeIn, timeOut, activity, date) {
      var totalTimeClocked = 0;
      if (timeIn !== "") {
        totalTimeClocked = this.calcTotalTime(timeIn, timeOut);
      }

      // clone first row and set each value
      var clone = $("#1").clone();
      clone.find(".name").text(name);
      if (timeOut !== ""){
        clone.find(".timeIn").val(this.convertMilitaryToStandard(timeIn));
      } else {
        clone.find(".timeIn").val("");
      }

      if (timeOut !== ""){
        clone.find(".timeOut").val(this.convertMilitaryToStandard(timeOut));
      } else {
        clone.find(".timeOut").val("");
      }

      console.log(totalTimeClocked.toFixed(1));
      clone.find(".total").text(totalTimeClocked.toFixed(1) + " hours");

      clone.attr("id", ++this.currentRow);

      clone.appendTo($("#hourContainer"));
    },

    calcTotalTime: function(timeIn, timeOut) {
      var timeInMinuteTotal = parseInt(timeIn.substring(0, 2)) * 60 + parseInt(timeIn.substring(3));
      var timeOutMinuteTotal;

      if (timeIn === null) {
        return null;
      }

      if(timeOut === "") {
        // calculate total hours to now
        console.log("Time out left empty");
        var date = new Date();
        timeOutMinuteTotal = date.getHours() * 60 + date.getMinutes();
      }else {
        timeOutMinuteTotal = parseInt(timeOut.substring(0, 2)) * 60 + parseInt(timeOut.substring(3));
      }

      var timeDiff = timeOutMinuteTotal - timeInMinuteTotal;

      if (timeInMinuteTotal > timeOutMinuteTotal) {
        // add 1 day to result because we have a negative time that passed 12 am
        timeDiff += (24 * 60);
      }

      return (timeDiff / 60);
    },

    convertMilitaryToStandard: function(s) {
      // given a time as hh:mm i.e. 12:34
      var hours = parseInt(s.substring(0, 2));
      var mins = s.substring(3);
      var result = "";

      if (hours < 12) {
        if (hours === 0) {
          hours = 12;
        }
        result += hours + ":" + mins + " A.M.";
      } else {
        if (hours !== 12) {
          hours -= 12;
        }
        result += hours + ":" + mins + " P.M.";
      }

      return result;
    },

    clearModal: function() {
      // clear the values from the modal except date
      $("#employeeName").val("");
      $("#timein").val("");
      $("#timeout").val("");
      $("#activity").val("");
    },

    generateCSV: function() {
      console.log("We generated a csv log statement!");
      var output = "Name,Time in,Time out,Activity,Date\n";
      for (var i = 0; i < this.testJson.length; i++){
        for (var j = 0; j < this.testJson[i].length; j++){
          output += "\"" + this.testJson[i][j] + "\"";
          if (j !== this.testJson[i].length - 1){
            output += ",";
          }
        }
        output += "\n";
      }
      var blob = new Blob([output], {type: "text/plain;charset=utf-8"});
      saveAs(blob, "ExcelData.txt");
    },

    deleteData: function() {
      localStorage.removeItem('employees');
    },

    clickHeroButton: async function() {
      // Scroll to the 'get started' section:
      $('html, body').animate({
        scrollTop: this.$find('[role="scroll-destination"]').offset().top
      }, 500);
    },

    // Private methods not tied to a particular DOM event are prefixed with _
    _setHeroHeight: function() {
      var $hero = this.$find('[full-page-hero]');
      var headerHeight = $('#page-header').outerHeight();
      var heightToSet = $(window).height();
      heightToSet = Math.max(heightToSet, 500);//« ensure min height of 500px - header height
      heightToSet = Math.min(heightToSet, 1000);//« ensure max height of 1000px - header height
      $hero.css('min-height', heightToSet - headerHeight+'px');
      this.heroHeightSet = true;
    },

  }
});
