/**
 * Security Settings
 * (sails.config.security)
 * how to deal with cross-origin requests (CORS) and which
 * routes require a CSRF token to be included with the request.*/

module.exports.security = {

  // cors: {
  //   allRoutes: false,
  //   allowOrigins: '*',
  //   allowCredentials: false,
  // },

  csrf: true

};
