/**
 * Session Configuration
 * (sails.config.session)
 *
 * Use the settings below to configure session integration in your app.
 * (for additional recommended settings, see `config/env/production.js`)
 *
 * For all available options, see:
 * https://sailsjs.com/config/session
 */

module.exports.session = {

  /* Session secret is automatically generated when your new app is created
  Replace at your own risk in production-- you will invalidate the cookies
  of your users, forcing them to log in again.*/
  secret: '712a385f7678a7a8d663d6e6dc356492',
  
  // Customize when built-in session support will be skipped.
  // isSessionDisabled: function (req){
  //   return !!req.path.match(req._sails.LOOKS_LIKE_ASSET_RX);
  // },

};
