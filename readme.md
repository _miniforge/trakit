## Trakit

### What is it:
Trakit is an open source time tracking tool

### Features:
Create Teams

---

Create Projects

---

Timesheets
Day & Week View
2 modes: Admin & User
Admin has the ability to adjust who worked the hours entered
Enter hours manually
Enter a start/end time and Trakit will calculate the hours
Select what team member the hours were completed by [ADMIN]

---

Dashboard / Analytics view (Future)

### Design Inspirations:
https://dribbble.com/shots/2466421-Designer-Time-Tracking-App-II
https://dribbble.com/shots/2058593-Designer-Time-Tracking-App
https://dribbble.com/shots/2074040-Designer-Time-Tracking-App
